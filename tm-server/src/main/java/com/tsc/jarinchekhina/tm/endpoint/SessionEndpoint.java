package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.ISessionEndpoint;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.dto.UserDTO;
import com.tsc.jarinchekhina.tm.model.Session;
import com.tsc.jarinchekhina.tm.model.User;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void closeSession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().close(session);
    }

    @NotNull
    @Override
    @WebMethod
    public UserDTO getUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return User.toDTO(serviceLocator.getSessionService().getUser(session));
    }

    @Nullable
    @Override
    @WebMethod
    public Session openSession(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) {
        return serviceLocator.getSessionService().open(login, password);
    }

}
