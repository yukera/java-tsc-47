package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.ISessionRepository;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.model.Session;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    public void add(@NotNull final Session session) {
        getEntityManager().persist(session);
    }

    @Override
    public void addAll(@NotNull Collection<Session> collection) {
        for (@NotNull Session session : collection) {
            add(session);
        }
    }

    public void update(@NotNull final Session session) {
        getEntityManager().merge(session);
    }

    @Override
    @SneakyThrows
    public void clear() {
        getEntityManager()
                .createQuery("DELETE FROM Session")
                .executeUpdate();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll() {
        return getEntityManager()
                .createQuery("SELECT e FROM Session e", Session.class)
                .getResultList();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session findById(@NotNull final String id) {
        @Nullable final List<Session> sessionList = getEntityManager()
                .createQuery("SELECT e FROM Session e WHERE e.id = :id", Session.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        if (sessionList == null || sessionList.isEmpty()) throw new AccessDeniedException();
        return sessionList.get(0);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findByUserId(@NotNull final String userId) {
        return getEntityManager()
                .createQuery("SELECT e FROM Session e WHERE e.user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final Session session) {
        removeById(session.getId());
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String id) {
        @NotNull final Session session = getEntityManager().getReference(Session.class, id);
        getEntityManager().remove(session);
    }

}
