package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.ITaskEndpoint;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.dto.TaskDTO;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.model.Session;
import com.tsc.jarinchekhina.tm.model.Task;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskDTO bindTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        return Task.toDTO(serviceLocator.getProjectTaskService().bindTaskByProjectId(session.getUser().getId(), projectId, taskId));
    }

    @Override
    @WebMethod
    public void changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().changeTaskStatusById(session.getUser().getId(), id, status);
    }

    @Override
    @WebMethod
    public void changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().changeTaskStatusByIndex(session.getUser().getId(), index, status);
    }

    @Override
    @WebMethod
    public void changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().changeTaskStatusByName(session.getUser().getId(), name, status);
    }

    @Override
    @WebMethod
    public void clearTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().clear(session.getUser().getId());
    }

    @Override
    @WebMethod
    public void createTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().create(session.getUser().getId(), name);
    }

    @Override
    @WebMethod
    public void createTaskWithDescription(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().create(session.getUser().getId(), name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        return Task.toDTO(serviceLocator.getProjectTaskService().findAllTaskByProjectId(session.getUser().getId(), projectId));
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        return Task.toDTO(serviceLocator.getTaskService().findAll(session.getUser().getId()));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskDTO findTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        return Task.toDTO(serviceLocator.getTaskService().findById(session.getUser().getId(), id));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskDTO findTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        return Task.toDTO(serviceLocator.getTaskService().findByIndex(session.getUser().getId(), index));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskDTO findTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        return Task.toDTO(serviceLocator.getTaskService().findByName(session.getUser().getId(), name));
    }

    @Override
    @WebMethod
    public void finishTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().finishTaskById(session.getUser().getId(), id);
    }

    @Override
    @WebMethod
    public void finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().finishTaskByIndex(session.getUser().getId(), index);
    }

    @Override
    @WebMethod
    public void finishTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().finishTaskByName(session.getUser().getId(), name);
    }

    @Override
    @WebMethod
    public void removeTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().removeById(session.getUser().getId(), id);
    }

    @Override
    @WebMethod
    public void removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().removeByIndex(session.getUser().getId(), index);
    }

    @Override
    @WebMethod
    public void removeTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().removeByName(session.getUser().getId(), name);
    }

    @Override
    @WebMethod
    public void startTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().startTaskById(session.getUser().getId(), id);
    }

    @Override
    @WebMethod
    public void startTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().startTaskByIndex(session.getUser().getId(), index);
    }

    @Override
    @WebMethod
    public void startTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().startTaskByName(session.getUser().getId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskDTO unbindTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        return Task.toDTO(serviceLocator.getProjectTaskService().unbindTaskByProjectId(session.getUser().getId(), taskId));
    }

    @Override
    @WebMethod
    public void updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().updateTaskById(session.getUser().getId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUser().getId(), Role.USER);
        serviceLocator.getTaskService().updateTaskByIndex(session.getUser().getId(), index, name, description);
    }

}
